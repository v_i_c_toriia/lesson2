package mortalWombat;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AgeCheckerTest {


    @Test
    public void testAgedUserCanPlay() {
        AgeChecker ageChecker = new AgeChecker();
        Assert.assertTrue(ageChecker.canPlayGame(22), "User too young");
    }

    @Test
    public void testThatTooYongUsersCanNotPlay() {
        AgeChecker ageChecker = new AgeChecker();
        Assert.assertFalse(ageChecker.canPlayGame(17), "User is not too young");
    }
}
